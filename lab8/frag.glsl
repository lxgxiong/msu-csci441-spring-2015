#version 330

in vec3 fcolor;
in vec2 fuv;
out vec4 color_out;

uniform sampler2D tex;

void main() {
  color_out = texture(tex,fuv).rrra;
}
