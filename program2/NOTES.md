Program 2
---------

Grade Breakdown
---------------

    Virtual trackball      - 20/20
    Diffuse shading        - 18/20 (see note 1)
    Option 1 (buttons)     - 10/10
    Option 2 (height)      - 10/10
    Option 3 (arms)        - 10/10
    Option 4 (army)        - 10/10
    General                - 10/20
    -------------------------------
                             88/100

Notes
-----
1. Make sure to normalize your light direction and normal vectors and clamp the dot product to between 0 and 1.
