#version 330

in vec3 fcolor;
in vec3 fposition;
in vec3 fnormal;

uniform vec3 lightPos;
uniform float ambient;
uniform float diff;

out vec4 color_out;

void main() {
  vec3 L = (lightPos- fposition);
  float diffuse = diff * dot(L,fnormal);
  color_out = vec4((diffuse + ambient) * fcolor, 1);
}
