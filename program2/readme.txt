Author: Lei Xiong

Instructions

1. Press Key_A to switch the snowman to an army of snowman.

3. Press Key_Up to wave arms go up.

4. Press key_Down to wave arms go down.

5. Press Key_T to taller the snowman.

6. Press Key_S to shorter the snowman.

7. The 4 configurations are: Change height of snowman, wave arms, add or remove buttons when height is changed, an army of snowman.

8. The height of snowman and the degree of arm waving have limits.