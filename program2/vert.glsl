#version 330


uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform mat4 trackball;

in vec3 position;
in vec3 color;
in vec3 normal;

out vec3 fcolor;
out vec3 fposition;
out vec3 fnormal;

void main() {
  gl_Position = projection * view *trackball * model * vec4(position, 1);
  fcolor = color;
  fposition = (model * vec4(position,1)).xyz;
  fnormal = (transpose(inverse(model)) * vec4(normal,0)).xyz;
}
