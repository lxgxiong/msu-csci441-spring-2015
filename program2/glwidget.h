#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <glm/glm.hpp>

#define GLM_FORCE_RADIANS

using glm::mat4;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core {
    Q_OBJECT

    public:
        GLWidget(QWidget *parent=0);
        ~GLWidget();

        GLuint loadShaders(const char* vertf, const char* fragf);
    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();

        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void keyPressEvent(QKeyEvent *event);

    private:
        void initializeCube();
        void renderCube(mat4 transform);
        void renderBase(mat4 transform);
        void renderBody(mat4 transform);
        void renderHead(mat4 transform);
        void renderMouth(mat4 transform);
        void renderLeftEye(mat4 transform);
        void renderRightEye(mat4 tranform);
        void renderLeftArm(mat4 transform);
        void renderRightArm(mat4 transform);
        void renderButtons(mat4 transform);

        GLuint cubeProg;
        GLuint cubeVao;
        GLint cubeProjMatrixLoc;
        GLint cubeViewMatrixLoc;
        GLint cubeModelMatrixLoc;
        GLint cubeTrackballMatrixLoc;

        void initializeGrid();
        void renderGrid();

        GLuint gridProg;
        GLuint gridVao;
        GLint gridProjMatrixLoc;
        GLint gridViewMatrixLoc;
        GLint gridModelMatrixLoc;
        GLint gridTrackballMatrixLoc;
        GLint lightLoc;

        mat4 projMatrix;
        mat4 viewMatrix;
        mat4 modelMatrix;
        mat4 trackballMatrix;

        int width;
        int height;
        float tall;
        float degree;
        bool armyMode;
        float move;

        glm::vec3 pointOnVirtualTrackball(const glm::vec2 &pt);
        glm::vec3 lastPt;
};

#endif
