HEADERS += glwidget.h 
SOURCES += glwidget.cpp main.cpp

QT += opengl
CONFIG -= app_bundle
CONFIG += console
INCLUDEPATH += "../include"

RESOURCES += \
    shaders.qrc

DISTFILES += \
    readme.txt \
    img01.jpg \
    screenshot.tiff
