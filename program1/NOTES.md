Program 1
---------

Grade Breakdown
---------------

    Position - 20/20
    Color    - 15/15
    Size     - 10/10
    Shape    - 10/10 
    Fill     - 15/15 
    Resize   - 10/10 
    General  - 13/20
    -------------------------------
               93/100

Notes
-----
1. File open dialog
2. Extra triangle shape
3. Didn't take out pixelRatio code
4. Lots of Qt warnings when sampling out of bounds of image
