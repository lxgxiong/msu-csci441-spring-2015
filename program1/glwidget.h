#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <glm/glm.hpp>

// glm by default uses degrees, but that functionality
// is deprecated so GLM_FORCE_RADIANS turns off some
// glm warnings
#define GLM_FORCE_RADIANS

using glm::vec2;
using glm::vec3;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core {
    Q_OBJECT

    public:
        GLWidget(QWidget *parent=0);
        ~GLWidget();

    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();

        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void keyPressEvent(QKeyEvent *event);

    private:
        GLuint loadShaders();
        static const GLchar* readShader(const char* filename);

        GLuint vao;
        GLuint program;
        GLuint positionBuffer;
        GLuint colorBuffer;
        bool outline;
        int drawModeIdx;
        GLint matrixPosition;

        std::vector<vec2> pts;
        std::vector<vec3> colors;
        int num_pts;
        float currentShapeSize;

        void drawSquare(float x,float y, float red, float g, float b);
        void drawTriangle(float x,float y, float r, float g, float b);
        void drawCircle(float x,float y,float r,float g, float b);
        void open();
        QImage image;
        void fill();
};

#endif
