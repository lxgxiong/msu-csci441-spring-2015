#version 330

out vec4 color_out;
in vec3 vColorOut;

void main(){
    color_out = vec4(vColorOut.r,vColorOut.g,vColorOut.b,1.0);
}
