#version 330

in vec2 position;
in vec3 color;
uniform mat4 projection;
out vec3 vColorOut;

void main() {
   gl_Position = projection * vec4(position.x, position.y, 0.0, 1.0);
   vColorOut = vec3(color.r,color.g,color.b);
}
