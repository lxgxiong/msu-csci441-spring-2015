#include "glwidget.h"
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <QTextStream>
#include <QImage>
#include <QFileDialog>
#include <QDir>
#include <QRgb>
#include <math.h>

using namespace std;
using namespace glm;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent), outline(false) {
    num_pts = 0;
    drawModeIdx = 0;
    currentShapeSize = 10.0f;
}

GLWidget::~GLWidget() {
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
    glUseProgram(program);
    glBindVertexArray(vao);
    switch(event->key()) {
        case Qt::Key_C:
            cout << "Cleared all the points." << endl;
            pts.clear();
            colors.clear();
            num_pts = 0;
            glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
            glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
            glBufferData(GL_ARRAY_BUFFER,0,NULL,GL_DYNAMIC_DRAW);
            update();
            break;
        case Qt::Key_F:
            fill();
            break;
        case Qt::Key_Up:
        if(currentShapeSize<=50)
            currentShapeSize++;
            break;
        case Qt::Key_Down:
        if(currentShapeSize>1)
            currentShapeSize--;
            break;
        case Qt::Key_Space:
            drawModeIdx++;
            if(drawModeIdx==3){
                drawModeIdx = 0;
            }
            break;
    }
}

void GLWidget::mouseMoveEvent(QMouseEvent *event){
    mousePressEvent(event);
}

void GLWidget::drawSquare(float x,float y,float red,float g, float b){
    float r = currentShapeSize;

    pts.push_back(vec2(x,y));
    pts.push_back(vec2(x-r,y-r));
    pts.push_back(vec2(x+r,y-r));

    pts.push_back(vec2(x,y));
    pts.push_back(vec2(x+r,y-r));
    pts.push_back(vec2(x+r,y+r));

    pts.push_back(vec2(x,y));
    pts.push_back(vec2(x+r,y+r));
    pts.push_back(vec2(x-r,y+r));

    pts.push_back(vec2(x,y));
    pts.push_back(vec2(x-r,y+r));
    pts.push_back(vec2(x-r,y-r));
    for(int i=0;i<12;i++){
        colors.push_back(vec3(red,g,b));
    }
    num_pts+=12;
}

void GLWidget::drawTriangle(float x, float y, float r, float g, float b){
    pts.push_back(vec2(x,y-currentShapeSize));
    pts.push_back(vec2(x-currentShapeSize*sin(M_PI/6.0),y+currentShapeSize*cos(M_PI/6.0)));
    pts.push_back(vec2(x+currentShapeSize*sin(M_PI/6.0),y+currentShapeSize*cos(M_PI/6.0)));
    num_pts+=3;
    colors.push_back(vec3(r,g,b));
    colors.push_back(vec3(r,g,b));
    colors.push_back(vec3(r,g,b));
}

void GLWidget::drawCircle(float x, float y, float r, float g, float b){
    int n = 180;
    for(int i=1;i<=n;i++){
        float angle=i*((M_PI*2)/n);
        float angle1;
        if(i!=360){
            angle1 = (i+1)*((M_PI*2)/n);
        }else{
            angle1 = 1*((M_PI*2)/n);
        }
        float x1 = x+ currentShapeSize*cos(angle);
        float y1 = y+ currentShapeSize*sin(angle);
        float x2 = x+ currentShapeSize*cos(angle1);
        float y2 = y+ currentShapeSize*sin(angle1);
        pts.push_back(vec2(x,y));
        pts.push_back(vec2(x1,y1));
        pts.push_back(vec2(x2,y2));
        num_pts+=3;
        colors.push_back(vec3(r,g,b));
        colors.push_back(vec3(r,g,b));
        colors.push_back(vec3(r,g,b));
    }
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
    glUseProgram(program);
    glBindVertexArray(vao);
    qreal pixelRatio = this->devicePixelRatio();
    QRgb c = image.pixel(event->x(),event->y());
    QColor c2(c);
    float x = event->x()*pixelRatio;
    float y = event->y()*pixelRatio;
    float r = (float)c2.red()/255.0;
    float g = (float)c2.green()/255.0;
    float b = (float)c2.blue()/255.0;

    if(drawModeIdx == 0){
        drawSquare(x,y,r,g,b);
    }else if(drawModeIdx==1){
        drawTriangle(x,y,r,g,b);
    }else if(drawModeIdx==2){
        drawCircle(x,y,r,g,b);
    }

    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, pts.size()*sizeof(vec2), &pts.front(), GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, colors.size()*sizeof(vec3), &colors.front(), GL_DYNAMIC_DRAW);
    update();
}

void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    open();

    //glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glPointSize(4.0f);

    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // Create a buffer on the GPU for position data
    glGenBuffers(1, &positionBuffer);

    // Upload the position data to the GPU, storing
    // it in the buffer we just allocated.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);



    // Load our vertex and fragment shaders into a program object
    // on the GPU
    program = loadShaders();

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");


    GLint projection = glGetUniformLocation(program, "projection");
    matrixPosition = projection;


    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

    GLint colorIndex = glGetAttribLocation(program,"color");
    glGenBuffers(1,&colorBuffer);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);

    glEnableVertexAttribArray(colorIndex);
    glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

}

void GLWidget::open(){
    QString filename = QFileDialog::getOpenFileName(this,tr("Open File"),QDir::currentPath());
    image = QImage(filename);
    cout << filename.toStdString().empty() << endl;
    if(filename.toStdString().empty()){
        cout << "You didn't open a picture. Please run this program again and open a picture." << endl;
        exit(0);
    }
}

void GLWidget::fill(){
    glUseProgram(program);
    glBindVertexArray(vao);
    for(int x=0;x<=this->width();x+=2*currentShapeSize){
        for(int y=0;y<=this->height();y+=2*currentShapeSize){
            QRgb c = image.pixel(x,y);
            QColor c2(c);
            float r = (float)c2.red()/255.0;
            float g = (float)c2.green()/255.0;
            float b = (float)c2.blue()/255.0;
            if(drawModeIdx==0){
                drawSquare(x,y,r,g,b);
            }else if(drawModeIdx==1){
                drawTriangle(x,y,r,g,b);
            }else if(drawModeIdx==2){
                drawCircle(x,y,r,g,b);
            }
        }
    }

    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, pts.size()*sizeof(vec2), &pts.front(), GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, colors.size()*sizeof(vec3), &colors.front(), GL_DYNAMIC_DRAW);
    update();
}

void GLWidget::resizeGL(int w, int h) {
    glUseProgram(program);
    glBindVertexArray(vao);
    glViewport(0,0,w,h);

    mat4 m = ortho(0.0f,(float)w,(float)h,0.0f);
    glUniformMatrix4fv(matrixPosition,1,GL_FALSE,glm::value_ptr(m));
    update();
}

void GLWidget::paintGL() {
    glUseProgram(program);
    glBindVertexArray(vao);
    glClear(GL_COLOR_BUFFER_BIT);

    // draw primitives based on the current draw mode
    glDrawArrays(GL_TRIANGLES, 0, num_pts);
}

// Copied from LoadShaders.cpp in the the oglpg-8th-edition.zip
// file provided by the OpenGL Programming Guide, 8th edition.
const GLchar* GLWidget::readShader(const char* filename) {
#ifdef WIN32
        FILE* infile;
        fopen_s( &infile, filename, "rb" );
#else
    FILE* infile = fopen( filename, "rb" );
#endif // WIN32

    if ( !infile ) {
#ifdef _DEBUG
        std::cerr << "Unable to open file '" << filename << "'" << std::endl;
#endif /* DEBUG */
        return NULL;
    }

    fseek( infile, 0, SEEK_END );
    int len = ftell( infile );
    fseek( infile, 0, SEEK_SET );

    GLchar* source = new GLchar[len+1];

    fread( source, 1, len, infile );
    fclose( infile );

    source[len] = 0;

    return const_cast<const GLchar*>(source);
}

GLuint GLWidget::loadShaders() {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(":/vert.glsl");
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    string vertSTLString = vertString.toStdString();

    cout << "Vertex Shader:" << endl;
    cout << vertSTLString << endl;

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(":/frag.glsl");
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    string fragSTLString = fragString.toStdString();

    cout << "Fragment Shader:" << endl;
    cout << fragSTLString << endl;

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);
    glUseProgram(program);

    return program;
}
