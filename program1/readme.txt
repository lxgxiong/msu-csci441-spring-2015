Author: Lei Xiong

Instructions

1. When run the program, the program will open a file browser dialog,
choose a image you want.

2. Press key_space to switch different brushes, there are 3 different
brush shapes, which are square, triangle, circle.

3. Press key_up and key_down to change the size of current brush

4. Press key_f to fill the whole window.

5. Press key_c to clear everything in the window.


writeup

This image that I turned in is a picture I took in front of the Johnstone
Center on campus. It was a fewdays after I first come to the US from China.
I hadn't seen such a beautiful sunset for a very long time because of the
air pollution in China. So I was very excited to see that beautiful sunset.
Besides, this picture is very colorful, so I think this image can show the
functionality of my program very well. So I choose this image.
