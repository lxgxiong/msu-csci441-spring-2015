#version 330

uniform vec3 lightPos;
uniform vec3 lightColor;
uniform float lightIntensity;

uniform vec3 ambientColor;
uniform vec3 diffuseColor;
uniform float shininess;
uniform vec3 halfvector;
uniform float strength;
uniform mat4 view;

in vec3 pos;
in vec3 norm;

out vec4 color_out;

void main() {
    mat4 invertView = inverse(view);
    vec3 L;
    vec3 N = normalize(norm);

    L = normalize((invertView*vec4(lightPos,1)).xyz-pos);
    vec3 V = normalize(invertView[3].xyz-pos);
    vec3 diffuse = diffuseColor*clamp(dot(N,L),0,1);

    float specular = max(0.0,dot(N,normalize(V+L)));

    specular=pow(specular,shininess);

    vec3 scatteredLight = ambientColor + lightColor*diffuse;
    vec3 reflectedLight = lightColor * specular * strength;




    color_out = vec4(lightIntensity*lightColor*diffuse+ambientColor+reflectedLight, 1);
    //color_out = vec4(lightIntensity*lightColor*diffuse+ambientColor, 1);
}
