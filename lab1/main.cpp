#include <QImage>
#include <QColor>
#include <iostream>
#include <vector>

/**
 *  Group members: Lei Xiong, Shengjie Chong
 */

using namespace std;

float dot(float x1, float y1, float x2, float y2){
    return x1*x2+y1*y2;
}

bool checkIfInside(float u, float v){
    return (u>=0) && (v>=0) && (u+v<1);
}

int main() {

    cout << "Enter 3 points (enter a point as x y r g b, put a blank between each number):" << endl;

    float x1,x2,x3,y1,y2,y3,r1,r2,r3,g1,g2,g3,b1,b2,b3;
    cin >> x1 >> y1 >> r1 >> g1 >> b1;
    cin >> x2 >> y2 >> r2 >> g2 >> b2;
    cin >> x3 >> y3 >> r3 >> g3 >> b3;


    //test data, comment the three line above and discomment the five lines below to test
//    x1=50, x2 = 600, x3 = 300;
//    y1=50,y2=20,y3=400;
//    r1=1,g1=0,b1=0;
//    r2=0,g2=1,b2=0;
//    r3=0,g3=0,b3=1;
    cout << "You entered:" << endl;
    cout << x1 << ","<< y1 << ":" << r1 << "," << g1 << ","<< b1 << endl;
    cout << x2 << ","<< y2 << ":" << r2 << "," << g2 << ","<< b2 << endl;
    cout << x3 << ","<< y3 << ":" << r3 << "," << g3 << ","<< b3 << endl;

    /*
      Prompt user for 3 points separated by whitespace.

      Part 1:
          You'll need to get the x and y coordinate as floating point values
          from the user for 3 points that make up a triangle.

      Part 3:
          You'll need to also request 3 colors from the user each having
          3 floating point values (red, green and blue) that range from 0 to 1.
    */

    // create an image 640 pixels wide by 480 pixels tall
    // with a standard 32 bit red, green, blue format
    QImage image(640, 480, QImage::Format_RGB32);

    for(int x=0;x<640;x++){
        for(int y=0;y<480;y++){
            float u,v;
            float dot00 = dot(x3-x1, y3-y1, x3-x1, y3-y1);
            float dot01 = dot(x3-x1, y3-y1, x2-x1, y2-y1);
            float dot02 = dot(x3-x1, y3-y1, x-x1, y-y1);
            float dot11 = dot(x2-x1, y2-y1, x2-x1, y2-y1);
            float dot12 = dot(x2-x1, y2-y1, x-x1, y-y1);

            float invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
            u = (dot11 * dot02 - dot01 * dot12) * invDenom;
            v = (dot00 * dot12 - dot01 * dot02) * invDenom;

            if(checkIfInside(u,v)){
                int r=0,g=0,b=0;
                r = (r1 *(1-u-v) +r2*v+r3*u)*255;
                g = (g1 *(1-u-v) +g2*v+g3*u)*255;
                b = (b1 *(1-u-v) +b2*v+b3*u)*255;

                image.setPixel(x,y,qRgb(r,g,b));
            }
        }
    }

    /* 
      Part 1:
          Calculate the bounding box of the 3 provided points and loop
          over each pixel in that box and set it to white using:

          image.setPixel(x,y, qRgb(255,255,255));

      Part 2:
          Modify your loop from part 1. Using barycentric coordinates, 
          determine if the pixel lies within the triangle defined by 
          the 3 provided points. If it is color it white, otherwise
          move on to the next pixel.

          For more on barycentric coordinates:
          http://en.wikipedia.org/wiki/Barycentric_coordinate_system

      Part 3:
          For each pixel in the triangle, calculate the color based on
          the calculated barycentric coordinates and the 3 provided
          colors. Your colors should have been entered as floating point
          numbers from 0 to 1. Since we have a 32 bit image, the red, 
          green and blue components range from 0 to 255. Be sure to make
          the conversion.
    */



    if(image.save("triangle.jpg",0,100)) {
        cout << "Output triangle.jpg" << endl;
    } else {
        cout << "Unable to save triangle.jpg" << endl;
    }
}

