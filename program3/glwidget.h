#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <QTimer>
#include <glm/glm.hpp>
#include <QOpenGLTexture>
#include <tinyobjloader/tiny_obj_loader.h>

#define GLM_FORCE_RADIANS

using glm::mat3;
using glm::mat4;
using glm::vec3;
using std::vector;

struct Particle {
    vec3 position;
    vec3 velocity;
    vec3 acceleration;
    float lifespan;
};

class ParticleSystem {
    public:
        ParticleSystem();
        ~ParticleSystem();

        void emitParticle(Particle p);
        void step(float dt);
        Particle* data();
        unsigned int numParticles();

    private:
        std::vector<Particle> particles;
};


class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core {
    Q_OBJECT

    public:
        GLWidget(QWidget *parent=0);
        ~GLWidget();

        GLuint loadShaders(const char* vertf, const char* fragf);
    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();

        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void keyPressEvent(QKeyEvent *event);
        void keyReleaseEvent(QKeyEvent *event);

    // Part 2 - add an animate slot

    private:
        void initializeCube();
        void initializeSmoothModel(const tinyobj::shape_t &shape,
                                             vector<vec3> &positions,
                                             vector<vec3> &normals,
                                             vector<unsigned int> &indices);
        void initializeModel(const char* filename);
        void renderBunny();

        void initializeOlaf(const char* filename);
        void renderOlaf();

        int numBunnyIndices;
        int numOlafIndices;


        void renderParticleSystem(ParticleSystem *ps);
        GLuint modelProg;
        GLuint modelSmoothVao;
        GLint modelProjMatrixLoc;
        GLint modelViewMatrixLoc;
        GLint modelModelMatrixLoc;



        ParticleSystem ps;
        GLuint psProg;
        GLuint psVao;
        GLint psProjMatrixLoc;
        GLint psViewMatrixLoc;
        GLint psModelMatrixLoc;
        GLint psTrackballMatrixLoc;
        GLuint particleBuffer;

        void initializeParticleSystem();

        GLuint olafProg;
        GLuint olafSmoothVao;
        GLint olafProjMatrixLoc;
        GLint olafViewMatrixLoc;
        GLint olafModelMatrixLoc;

        GLint modelLightPosLoc;
        GLint modelLightColorLoc;
        GLint modelLightIntensityLoc;

        GLint modelDiffuseColorLoc;
        GLint modelAmbientColorLoc;

        GLint modelLightShininessLoc;
        GLint modelLightStrengthLoc;
        GLint modelLightHalfVector;

        GLint cubeLightPosLoc;
        GLint cubeLightColorLoc;
        GLint cubeLightIntensityLoc;

        GLint cubeDiffuseColorLoc;
        GLint cubeAmbientColorLoc;

        GLint cubeLightShininessLoc;
        GLint cubeLightStrengthLoc;
        GLint cubeLightHalfVector;

        GLuint gridProg;
        GLuint gridVao;
        GLint gridProjMatrixLoc;
        GLint gridViewMatrixLoc;
        GLint gridModelMatrixLoc;

        void renderCube(mat4 transform);
        void renderGround();
        void renderBase(mat4 transform);
        void renderBody(mat4 transform);
        void renderHead(mat4 transform);
        void renderMouth(mat4 transform);
        void renderLeftEye(mat4 transform);
        void renderRightEye(mat4 tranform);
        void renderLeftArm(mat4 transform);
        void renderRightArm(mat4 transform);
        void renderButtons(mat4 transform);
        void renderHouse();
        void renderBox();
        void renderFaces(mat4 transform);
        void renderSnowmen();
        void renderTree();
        void renderDesk(mat4 transform);
        void renderChair(mat4 transform);
        void renderBookShelf(mat4 transform);
        void renderTruck(mat4 transform);
        void renderFence(mat4 transform);
        void renderSwing(mat4 transform);

        void animateParticles();

        float swingDegree;
        float swingTrans;
        int swingIndex;
        bool isWeatherOpen;

        float time;
        mat4 eulerLerp;
        mat4 matrixLerp;
        vector<vec3> rotations;

        void initializeGrid();
        void renderGrid();

        GLuint cubeProg;
        GLuint cubeVao;
        GLint cubeProjMatrixLoc;
        GLint cubeViewMatrixLoc;
        GLint cubeModelMatrixLoc;
        GLuint textureObject;

        mat4 projMatrix;
        mat4 viewMatrix;
        mat4 modelMatrix;

        float yaw;
        float pitch;

        mat4 yawMatrix;
        mat4 pitchMatrix;

        QTimer *timer;

        int width;
        int height;

        bool forward;
        bool back;
        bool left;
        bool right;
        bool up;
        bool down;

        vec3 position;
        vec3 velocity;
        mat4 orientation;

        glm::vec2 lastPt;
        void updateView();

        QOpenGLTexture *tex;
        QOpenGLTexture *snowTex;
        QOpenGLTexture *brickTex;
        QOpenGLTexture *boxTex;
        QOpenGLTexture *treeTex;
        QOpenGLTexture *leavesTex;
        QOpenGLTexture *doorTex;

        float tall;
        float degree;
        bool armyMode;
        float move;

    public slots:
        void animate();
};

#endif
