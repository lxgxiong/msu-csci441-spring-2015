Operating Instructions:
1. Press Key W to move forward
2. Press Key S to move backward
3. Press Key a to move left
4. Press Key d to move right
5. Press Key shift to move down
6. Press Key space to move up


Extra Imformation:
1. I used the 2 days delay policy for this program
2. I intentionally left the screwed up rabbit bunny in my program,
    I could just remove it, but I want ask you how can I make it work.
    So hope you don't take out points for this, this doesn't count for
    my objects number.


Object descriptions:
1. 10 differet(height, buttons and arm angles)  snowmen.
2. A table made by cubes
3. A chair made by cubes
4. A truck made by cubes
5. A book shelf made by cubes
6. A set of fences made by cubes
7. A tree made by cubes
8. An animated swing made by cubes
9. A treature box made by cube
10. An imported object( which is the ring downloaded from your url
11. A simple house(with only a door) made by cubes

Program descriptions:
1. My light condition is just 1 stable(not moving) light in the world space.
2. I have used a couple different materials for all objects.
3. some of my objects used textures.
4. I used a little animation for the swing and the ring

Extra credit:
1. I did the particle system which is a raining animation.
2. Press Key_G to stop/start raining.
