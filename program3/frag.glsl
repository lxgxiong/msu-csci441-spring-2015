#version 330

uniform sampler2D tex;

in vec3 fcolor;
in vec2 fuv;

in vec3 pos;
in vec3 norm;

uniform vec3 lightPos;
uniform vec3 lightColor;
uniform float lightIntensity;

uniform vec3 ambientColor;
uniform vec3 diffuseColor;
uniform float shininess;
uniform vec3 halfvector;
uniform float strength;

out vec4 color_out;

void main() {
    vec3 L = normalize(lightPos-pos);
    vec3 N = normalize(norm);

    vec3 V = normalize(-pos);
    vec3 diffuse = diffuseColor*clamp(dot(N,L),0,1);

    float specular = max(0.0,dot(N,normalize(V+L)));

    specular=pow(specular,shininess);

    vec3 scatteredLight = ambientColor + lightIntensity * lightColor*diffuse;
    vec3 reflectedLight = lightColor * specular * strength;

    vec4 texcolor = texture(tex,fuv);
    color_out =vec4(ambientColor,1) + (vec4(diffuse,1) * vec4(texcolor.rgb,1.0)) + (specular * texcolor.a);
}
