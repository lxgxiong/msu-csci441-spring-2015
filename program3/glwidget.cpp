#include "glwidget.h"
#include <iostream>
#include <QOpenGLTexture>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QTextStream>
#include <QDir>
#include <QRect>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using glm::inverse;
using glm::vec2;
using glm::vec3;
using glm::mat3;
using glm::mat4;
using glm::perspective;
using glm::normalize;
using glm::length;
using glm::cross;
using glm::dot;
using glm::rotate;
using glm::value_ptr;
using glm::lookAt;

using namespace std;

ParticleSystem::ParticleSystem() {
}

ParticleSystem::~ParticleSystem() {
}

void ParticleSystem::emitParticle(Particle p) {
    particles.push_back(p);
}

Particle* ParticleSystem::data() {
    return &particles[0];
}

unsigned int ParticleSystem::numParticles() {
    return particles.size();
}

bool isDead(const Particle &p) {
    return p.lifespan <= 0;
}

void ParticleSystem::step(float dt) {
    std::vector<Particle>::iterator itr = particles.begin();
    while(itr != particles.end()) {

        itr->position += itr->velocity*dt;
        itr->velocity += itr->acceleration*dt;
        itr->lifespan -= dt;

        itr++;
    }
    particles.erase(remove_if(particles.begin(), particles.end(), isDead), particles.end());
}


GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) {
    timer =new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(animate()));
    timer->start(16);
    forward=false;
    back=false;
    left=false;
    right=false;
    up=false;
    down=false;

    tall=2.0f;
    degree=70.0f;
    armyMode=false;
    move = 0;

    eulerLerp = mat4(1.0f);

    rotations.push_back(vec3(0, 0, 0));
    rotations.push_back(vec3(M_PI/3, M_PI/2, -4*M_PI));
    rotations.push_back(vec3(M_PI/3, M_PI/2, -4*M_PI));


    swingDegree = -30;
    swingIndex = 1;
    swingTrans = swingIndex*0.03;

    isWeatherOpen= true;

}

GLWidget::~GLWidget() {
}

void GLWidget::animateParticles(){
    float dt = .016;

    ps.step(dt);
    update();
}

void GLWidget::animate(){
    float dt = 0.16;
    float speed = 1;


    if(isWeatherOpen){
        for(int i=0;i<100;i++){
            Particle p;
            p.position = vec3(rand()%200-100,rand() % 50,rand()%200-100);
            p.velocity = vec3(5,-5,5);
            p.acceleration = vec3(0,-6,0);
            p.lifespan = 1.6;

            ps.emitParticle(p);

        }
    }

    ps.step(dt);

    vec3 forwardVec = -vec3(yawMatrix[2]);
    vec3 rightVec = vec3(orientation[0]);
    vec3 upVec = vec3(0,1,0);
    velocity=vec3(0,0,0);
    if(forward){
        velocity +=forwardVec*speed;
    }

    if(back){
        velocity -=forwardVec*speed;
    }

    if(right){
        velocity+=rightVec*speed;
    }
    if(left){
        velocity-=rightVec*speed;
    }
    if(up){
        velocity+=upVec*speed;
    }
    if(down){
        velocity-=upVec*speed;
    }

    if(length(velocity)>0){
        velocity = normalize(velocity);
    }
    position +=velocity*speed *dt;


    time += dt;
    if(time > 6*(rotations.size()-1)) {
        time = 0;
    }

    float t = fmin(time/(2*(rotations.size()-1)),1);



    unsigned int fromIndex = t*(rotations.size()-1);
    unsigned int toIndex = fromIndex+1;
    if(toIndex > rotations.size()-1) {
        toIndex = rotations.size()-1;
    }

    t = t*(rotations.size()-1)-(int)(t*(rotations.size()-1));

    // Euler angle representations of
    vec3 from = rotations[fromIndex];
    vec3 to = rotations[toIndex];
    vec3 euler = (1-t)*from+t*to;

    glm::mat4 fromMat = glm::rotate(mat4(1.0f),from.z,vec3(0,0,1))*
            glm::rotate(mat4(1.0f),from.y,vec3(0,1,0))*
            glm::rotate(mat4(1.0),from.x,vec3(1,0,0));

    glm::mat4 toMat = glm::rotate(mat4(1.0f),to.z,vec3(0,0,1))*
            glm::rotate(mat4(1.0f),to.y,vec3(0,1,0))*
            glm::rotate(mat4(1.0),to.x,vec3(1,0,0));

    matrixLerp = (1-t)*fromMat+t*toMat;

    eulerLerp = glm::rotate(mat4(1.0f),euler.z,vec3(0,0,1))*
            glm::rotate(mat4(1.0f),euler.y,vec3(0,1,0))*
            glm::rotate(mat4(1.0),euler.x,vec3(1,0,0));

    if(swingDegree>=30){
        swingIndex=-1;
    }
    if(swingDegree<=-30){
        swingIndex=1;
    }

    swingDegree+=swingIndex;
    swingTrans = swingDegree*-0.01;

    updateView();
    update();
}


void GLWidget::initializeParticleSystem() {
    glGenVertexArrays(1, &psVao);
    glBindVertexArray(psVao);

    glGenBuffers(1, &particleBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, particleBuffer);
    glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_STREAM_DRAW);

    GLuint program = loadShaders(":/ps_vert.glsl", ":/ps_frag.glsl");
    glUseProgram(program);
    psProg = program;

    glBindBuffer(GL_ARRAY_BUFFER, particleBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), 0);

    psProjMatrixLoc = glGetUniformLocation(program, "projection");
    psViewMatrixLoc = glGetUniformLocation(program, "view");
    psModelMatrixLoc = glGetUniformLocation(program, "model");
    psTrackballMatrixLoc = glGetUniformLocation(program, "trackball");

    mat4 identity = mat4(1.0f);
    glUniformMatrix4fv(psViewMatrixLoc, 1, false, value_ptr(identity));
    glUniformMatrix4fv(psModelMatrixLoc, 1, false, value_ptr(identity));
    glUniformMatrix4fv(psTrackballMatrixLoc, 1, false, value_ptr(identity));
}


void GLWidget::initializeGrid() {
    glGenVertexArrays(1, &gridVao);
    glBindVertexArray(gridVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    vec3 pts[84];
    for(int i = -10; i <= 10; i++) {

        pts[2*(i+10)] = vec3(i, -.5f, 10);
        pts[2*(i+10)+1] = vec3(i, -.5f, -10);

        pts[2*(i+10)+42] = vec3(10,-.5f, i);
        pts[2*(i+10)+43] = vec3(-10,-.5f, i);
    }

    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/grid_vert.glsl", ":/grid_frag.glsl");
    glUseProgram(program);
    gridProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    gridProjMatrixLoc = glGetUniformLocation(program, "projection");
    gridViewMatrixLoc = glGetUniformLocation(program, "view");
    gridModelMatrixLoc = glGetUniformLocation(program, "model");
}

void GLWidget::renderGrid() {
    glUseProgram(gridProg);
    glBindVertexArray(gridVao);
    glDrawArrays(GL_LINES, 0, 84);
}

void GLWidget::initializeCube() {
    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &cubeVao);
    glBindVertexArray(cubeVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    GLuint colorBuffer;
    glGenBuffers(1, &colorBuffer);

    GLuint uvBuffer;
    glGenBuffers(1, &uvBuffer);

    GLuint indexBuffer;
    glGenBuffers(1, &indexBuffer);

    GLuint normalBuffer;
    glGenBuffers(1,&normalBuffer);

    vec3 pts[] = {
        // top
        vec3(1,1,1),    // 0
        vec3(1,1,-1),   // 1
        vec3(-1,1,-1),  // 2
        vec3(-1,1,1),   // 3

        // bottom
        vec3(1,-1,1),   // 4
        vec3(-1,-1,1),  // 5
        vec3(-1,-1,-1), // 6
        vec3(1,-1,-1),  // 7

        // front
        vec3(1,1,1),    // 8
        vec3(-1,1,1),   // 9
        vec3(-1,-1,1),  // 10
        vec3(1,-1,1),   // 11

        // back
        vec3(-1,-1,-1), // 12
        vec3(-1,1,-1),  // 13
        vec3(1,1,-1),   // 14
        vec3(1,-1,-1),  // 15

        // right
        vec3(1,-1,1),   // 16
        vec3(1,-1,-1),  // 17
        vec3(1,1,-1),   // 18
        vec3(1,1,1),     // 19

        // left
        vec3(-1,-1,1),  // 20
        vec3(-1,1,1),   // 21
        vec3(-1,1,-1),  // 22
        vec3(-1,-1,-1) // 23

    };

    for(int i = 0; i < 24; i++) {
        pts[i] *= .5;
    }

    vec3 colors[] = {
        // top
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),
        vec3(0,1,0),

        // bottom
        vec3(0,.5f,0),
        vec3(0,.5f,0),
        vec3(0,.5f,0),
        vec3(0,.5f,0),

        // front
        vec3(0,0,1),
        vec3(0,0,1),
        vec3(0,0,1),
        vec3(0,0,1),

        // back
        vec3(0,0,.5f),
        vec3(0,0,.5f),
        vec3(0,0,.5f),
        vec3(0,0,.5f),

        // right
        vec3(1,0,0),
        vec3(1,0,0),
        vec3(1,0,0),
        vec3(1,0,0),


        // left
        vec3(.5f,0,0),
        vec3(.5f,0,0),
        vec3(.5f,0,0),
        vec3(.5f,0,0)
    };

    vec2 uvs[] = {
        // top
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // bottom
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // front
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),
        vec2(0,0),

        // back
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // right
        vec2(1,1),
        vec2(1,0),
        vec2(0,0),
        vec2(0,1),

        // left
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0)

    };

    vec3 norm[] = {
        // top
        vec3(0,1,0),    // 0
        vec3(0,1,0),   // 1
        vec3(0,1,0),  // 2
        vec3(0,1,0),   // 3

        // bottom
        vec3(0,-1,0),   // 4
        vec3(0,-1,0),  // 5
        vec3(0,-1,0), // 6
        vec3(0,-1,0),  // 7

        // front
        vec3(0,0,1),    // 8
        vec3(0,0,1),   // 9
        vec3(0,0,1),  // 10
        vec3(0,0,1),   // 11

        // back
        vec3(0,0,-1), // 12
        vec3(0,0,-1),  // 13
        vec3(0,0,-1),   // 14
        vec3(0,0,-1),  // 15

        // right
        vec3(1,0,0),   // 16
        vec3(1,0,0),  // 17
        vec3(1,0,0),   // 18
        vec3(1,0,0),     // 19

        // left
        vec3(-1,0,0),  // 20
        vec3(-1,0,0),   // 21
        vec3(-1,0,0),  // 22
        vec3(-1,0,0) // 23

    };

    GLuint restart = 0xFFFFFFFF;
    GLuint indices[] = {
        0,1,2,3, restart,
        4,5,6,7, restart,
        8,9,10,11, restart,
        12,13,14,15, restart,
        16,17,18,19, restart,
        20,21,22,23
    };

    // Upload the position data to the GPU, storing
    // it in the buffer we just allocated.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(uvs), uvs, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);


    glBindBuffer(GL_ARRAY_BUFFER,normalBuffer);
    glBufferData(GL_ARRAY_BUFFER,sizeof(norm),norm,GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/vert.glsl", ":/frag.glsl");
    glUseProgram(program);
    cubeProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    GLint colorIndex = glGetAttribLocation(program, "color");
    glEnableVertexAttribArray(colorIndex);
    glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    GLint uvIndex = glGetAttribLocation(program, "uv");
    glEnableVertexAttribArray(uvIndex);
    glVertexAttribPointer(uvIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);


    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    GLint normIndex = glGetAttribLocation(program, "normal");
    glEnableVertexAttribArray(normIndex);
    glVertexAttribPointer(normIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);


    QImage img(":/textures/grass.jpg");
    tex = new QOpenGLTexture(img);

    QImage img1(":/textures/snow.jpg");
    snowTex = new QOpenGLTexture(img1);

    QImage img2(":/textures/brick.jpg");
    brickTex = new QOpenGLTexture(img2);

    QImage img3(":/textures/box.jpg");
    boxTex = new QOpenGLTexture(img3);

    QImage img4(":/textures/tree1.jpg");
    treeTex = new QOpenGLTexture(img4);

    QImage img5(":/textures/leaves.jpg");
    leavesTex = new QOpenGLTexture(img5);

    QImage img6(":/textures/door.jpg");
    doorTex = new QOpenGLTexture(img6);

    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA,img.width(),img.height(),0,GL_RGBA,GL_UNSIGNED_INT_8_8_8_8,img.bits());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    cubeProjMatrixLoc = glGetUniformLocation(program, "projection");
    cubeViewMatrixLoc = glGetUniformLocation(program, "view");
    cubeModelMatrixLoc = glGetUniformLocation(program, "model");

    cubeDiffuseColorLoc = glGetUniformLocation(program, "diffuseColor");
    cubeAmbientColorLoc = glGetUniformLocation(program, "ambientColor");

    cubeLightPosLoc = glGetUniformLocation(program, "lightPos");
    cubeLightColorLoc = glGetUniformLocation(program, "lightColor");
    cubeLightIntensityLoc = glGetUniformLocation(program, "lightIntensity");
    cubeLightShininessLoc = glGetUniformLocation(program,"shininess");
    cubeLightStrengthLoc = glGetUniformLocation(program,"strength");
    cubeLightHalfVector = glGetUniformLocation(program,"halfvector");


    glUniform3f(cubeLightPosLoc, -10,20,-5);
    glUniform3f(cubeLightColorLoc, 1,1,1);
    glUniform1f(cubeLightIntensityLoc, 1);

    glUniform3f(cubeAmbientColorLoc, 0, 0, .2);
    glUniform3f(cubeDiffuseColorLoc, .25, .8, 1);
    glUniform1f(cubeLightShininessLoc,10);
    glUniform1f(cubeLightStrengthLoc,1);
    glUniform3f(cubeLightHalfVector,0,1,1);
}

void GLWidget::initializeSmoothModel(const tinyobj::shape_t &shape,
                                     vector<vec3> &positions,
                                     vector<vec3> &normals,
                                     vector<unsigned int> &indices) {
    positions.clear();
    normals.clear();
    for(size_t v=0;v<shape.mesh.positions.size()/3;v++){
        float x = shape.mesh.positions[3*v+0];
        float y = shape.mesh.positions[3*v+1];
        float z = shape.mesh.positions[3*v+2];

        positions.push_back(vec3(x,y,z));
        normals.push_back(vec3(0,0,0));
    }

    for(size_t tri = 0; tri < shape.mesh.indices.size() / 3; tri++) {
        // Here we're getting the indices for the current triangle
        unsigned int ind0 = shape.mesh.indices[3*tri+0];
        unsigned int ind1 = shape.mesh.indices[3*tri+1];
        unsigned int ind2 = shape.mesh.indices[3*tri+2];

        // Using those indices we can get the three points of the triangle
        vec3 p0 = vec3(shape.mesh.positions[3*ind0+0],
                       shape.mesh.positions[3*ind0+1],
                       shape.mesh.positions[3*ind0+2]);

        vec3 p1 = vec3(shape.mesh.positions[3*ind1+0],
                       shape.mesh.positions[3*ind1+1],
                       shape.mesh.positions[3*ind1+2]);

        vec3 p2 = vec3(shape.mesh.positions[3*ind2+0],
                       shape.mesh.positions[3*ind2+1],
                       shape.mesh.positions[3*ind2+2]);

        vec3 n;

        vec3 a = p0-p1;
        vec3 b = p0-p2;
        n=normalize(cross(a,b));

        normals[ind0]+=n;
        normals[ind1]+=n;
        normals[ind2]+=n;

        indices.push_back(ind0);
        indices.push_back(ind1);
        indices.push_back(ind2);
    }

    for(unsigned long i =0;i<normals.size();i++){
        normals[i] = normalize(normals[i]);
    }
}

void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glPointSize(4.0f);

    glEnable(GL_DEPTH_TEST);

    GLuint restart = 0xFFFFFFFF;
    glPrimitiveRestartIndex(restart);
    glEnable(GL_PRIMITIVE_RESTART);


    initializeModel(":/models/bunny.obj");



    initializeOlaf(":/models/ring.obj");
    initializeCube();

    initializeParticleSystem();

    viewMatrix = glm::lookAt(vec3(0,0,0),vec3(0,0,0),vec3(0,1,0));
    modelMatrix = mat4(1.0f);

    glUseProgram(cubeProg);
    glUniformMatrix4fv(cubeViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(cubeModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(modelProg);
    glUniformMatrix4fv(modelViewMatrixLoc, 1, false, value_ptr(lookAt(vec3(0,0,-50),vec3(0,0,0),vec3(0,1,0))));
    glUniformMatrix4fv(modelModelMatrixLoc, 1, false, value_ptr(modelMatrix));


    glUseProgram(olafProg);
    glUniformMatrix4fv(olafViewMatrixLoc, 1, false, value_ptr(lookAt(vec3(0,0,-50),vec3(0,0,0),vec3(0,1,0))));
    glUniformMatrix4fv(olafModelMatrixLoc, 1, false, value_ptr(modelMatrix));


    glUseProgram(gridProg);
    glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(gridModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(psProg);
    glUniformMatrix4fv(psViewMatrixLoc, 1, false, value_ptr(lookAt(vec3(0,0,-10),vec3(0,0,0),vec3(0,1,0))));

}


void GLWidget::initializeOlaf(const char* filename){
        vector<tinyobj::shape_t> shapes;
        vector<tinyobj::material_t> materials;

        QFile file(filename);
        std::string err = tinyobj::LoadObj(shapes, materials, file);

        if(!err.empty()) {
            cerr << err << endl;
            exit(1);
        }

        cout << "# of shapes: " << shapes.size() << endl;
        cout << "# of materials: " << materials.size() << endl;

        vector<vec3> smooth_positions;
        vector<vec3> smooth_normals;
        vector<unsigned int> smooth_indices;

    // Use the first mesh in the obj file and to initialize positions, normals and indices
    // for smooth shading
        initializeSmoothModel(shapes[0], smooth_positions, smooth_normals, smooth_indices);

        numOlafIndices = smooth_indices.size();

        // Upload all our data to buffers

        GLuint bunnyPositionBuffer;
        glGenBuffers(1, &bunnyPositionBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, bunnyPositionBuffer);
        glBufferData(GL_ARRAY_BUFFER, smooth_positions.size()*sizeof(vec3), &smooth_positions[0], GL_STATIC_DRAW);

        GLuint bunnyNormalBuffer;
        glGenBuffers(1, &bunnyNormalBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, bunnyNormalBuffer);
        glBufferData(GL_ARRAY_BUFFER, smooth_normals.size()*sizeof(vec3), &smooth_normals[0], GL_STATIC_DRAW);

        GLuint bunnyIndexBuffer;
        glGenBuffers(1, &bunnyIndexBuffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bunnyIndexBuffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, smooth_indices.size()*sizeof(unsigned int), &smooth_indices[0], GL_STATIC_DRAW);

        // load our shaders
        GLuint program = loadShaders(":/olaf_vert.glsl", ":/olaf_frag.glsl");
        glUseProgram(program);
        olafProg = program;

        // get some variable positions
        GLint positionIndex = glGetAttribLocation(program, "position");
        GLint normalIndex = glGetAttribLocation(program, "normal");

        // bind our buffers to vertex array objects and shader attributes
        glGenVertexArrays(1, &olafSmoothVao);
        glBindVertexArray(olafSmoothVao);

        glBindBuffer(GL_ARRAY_BUFFER, bunnyPositionBuffer);
        glEnableVertexAttribArray(positionIndex);
        glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, bunnyNormalBuffer);
        glEnableVertexAttribArray(normalIndex);
        glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bunnyIndexBuffer);

        olafProjMatrixLoc = glGetUniformLocation(program, "projection");
        olafViewMatrixLoc = glGetUniformLocation(program, "view");
        olafModelMatrixLoc = glGetUniformLocation(program, "model");

        GLuint diffuseColorLoc = glGetUniformLocation(program, "diffuseColor");
        GLuint ambientColorLoc = glGetUniformLocation(program, "ambientColor");

        GLuint lightPosLoc = glGetUniformLocation(program, "lightPos");
        GLuint lightColorLoc = glGetUniformLocation(program, "lightColor");
        GLuint lightIntensityLoc = glGetUniformLocation(program, "lightIntensity");
        GLuint lightShininessLoc = glGetUniformLocation(program,"shininess");
        GLuint lightStrengthLoc = glGetUniformLocation(program,"strength");
        GLuint lightHalfVector = glGetUniformLocation(program,"halfvector");


        glUniform3f(lightPosLoc, 0,5,0);
        glUniform3f(lightColorLoc, 1,1,1);
        glUniform1f(lightIntensityLoc, 1);

        glUniform3f(ambientColorLoc, 0.05, 0.05, 0.05);
        glUniform3f(diffuseColorLoc, 1, .6, .6);
        glUniform1f(lightShininessLoc,1000);
        glUniform1f(lightStrengthLoc,1);
        glUniform3f(lightHalfVector,0,1,1);
}

void GLWidget::initializeModel(const char* filename) {
    vector<tinyobj::shape_t> shapes;
    vector<tinyobj::material_t> materials;

    QFile file(filename);
    std::string err = tinyobj::LoadObj(shapes, materials, file);

    if(!err.empty()) {
        cerr << err << endl;
        exit(1);
    }

    cout << "# of shapes: " << shapes.size() << endl;
    cout << "# of materials: " << materials.size() << endl;

    vector<vec3> smooth_positions;
    vector<vec3> smooth_normals;
    vector<unsigned int> smooth_indices;

// Use the first mesh in the obj file and to initialize positions, normals and indices
// for smooth shading
    initializeSmoothModel(shapes[0], smooth_positions, smooth_normals, smooth_indices);

    numBunnyIndices = smooth_indices.size();

    // Upload all our data to buffers

    GLuint bunnyPositionBuffer;
    glGenBuffers(1, &bunnyPositionBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, bunnyPositionBuffer);
    glBufferData(GL_ARRAY_BUFFER, smooth_positions.size()*sizeof(vec3), &smooth_positions[0], GL_STATIC_DRAW);

    GLuint bunnyNormalBuffer;
    glGenBuffers(1, &bunnyNormalBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, bunnyNormalBuffer);
    glBufferData(GL_ARRAY_BUFFER, smooth_normals.size()*sizeof(vec3), &smooth_normals[0], GL_STATIC_DRAW);

    GLuint bunnyIndexBuffer;
    glGenBuffers(1, &bunnyIndexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bunnyIndexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, smooth_indices.size()*sizeof(unsigned int), &smooth_indices[0], GL_STATIC_DRAW);

    // load our shaders
    GLuint program = loadShaders(":/model_vert.glsl", ":/model_frag.glsl");
    glUseProgram(program);
    modelProg = program;

    // get some variable positions
    GLint positionIndex = glGetAttribLocation(program, "position");
    GLint normalIndex = glGetAttribLocation(program, "normal");

    // bind our buffers to vertex array objects and shader attributes
    glGenVertexArrays(1, &modelSmoothVao);
    glBindVertexArray(modelSmoothVao);

    glBindBuffer(GL_ARRAY_BUFFER, bunnyPositionBuffer);
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, bunnyNormalBuffer);
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bunnyIndexBuffer);

    modelProjMatrixLoc = glGetUniformLocation(program, "projection");
    modelViewMatrixLoc = glGetUniformLocation(program, "view");
    modelModelMatrixLoc = glGetUniformLocation(program, "model");

    modelDiffuseColorLoc = glGetUniformLocation(program, "diffuseColor");
    modelAmbientColorLoc = glGetUniformLocation(program, "ambientColor");

    modelLightPosLoc = glGetUniformLocation(program, "lightPos");
    modelLightColorLoc = glGetUniformLocation(program, "lightColor");
    modelLightIntensityLoc = glGetUniformLocation(program, "lightIntensity");
    modelLightShininessLoc = glGetUniformLocation(program,"shininess");
    modelLightStrengthLoc = glGetUniformLocation(program,"strength");
    modelLightHalfVector = glGetUniformLocation(program,"halfvector");


    glUniform3f(modelLightPosLoc, -10,15,3);
    glUniform3f(modelLightColorLoc, 1,1,1);
    glUniform1f(modelLightIntensityLoc, 1);

    glUniform3f(modelAmbientColorLoc, 0, 0, .2);
    glUniform3f(modelDiffuseColorLoc, .25, .8, 1);
    glUniform1f(modelLightShininessLoc,1000);
    glUniform1f(modelLightStrengthLoc,1);
    glUniform3f(modelLightHalfVector,0,1,1);


}

void GLWidget::resizeGL(int w, int h) {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    width = w;
    height = h;

    float aspect = (float)w/h;

    projMatrix = perspective(45.0f, aspect, .01f, 100.0f);

    glUseProgram(cubeProg);
    glUniformMatrix4fv(cubeProjMatrixLoc, 1, false, value_ptr(projMatrix));

    glUseProgram(modelProg);
    glUniformMatrix4fv(modelProjMatrixLoc, 1, false, value_ptr(projMatrix));

    glUseProgram(olafProg);
    glUniformMatrix4fv(olafProjMatrixLoc, 1, false, value_ptr(projMatrix));

    glUseProgram(gridProg);
    glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(gridModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(psProg);
    glUniformMatrix4fv(psProjMatrixLoc, 1, false, value_ptr(projMatrix));

}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    renderGrid();
    renderGround();
    renderHouse();
    renderSnowmen();
    renderTree();
    renderOlaf();
    renderBunny();
    renderBox();
    renderDesk(glm::translate(mat4(1.0f),vec3(-5,0,5)));
    renderChair(glm::translate(mat4(1.0f),vec3(-5,0,6.5)));
    renderBookShelf(glm::translate(mat4(1.0f),vec3(-1,0,6.5)));

    for(int i=-30;i<30;i++){
        renderFence(glm::translate(mat4(1.0f),vec3(-20,0,i))*glm::scale(mat4(1.0f),vec3(2,2,2)));
        renderFence(glm::translate(mat4(1.0f),vec3(30,0,i))*glm::scale(mat4(1.0f),vec3(2,2,2)));
    }
    for(int i=-20;i<30;i++){
        renderFence(glm::translate(mat4(1.0f),vec3(i,0,-30))* glm::rotate(mat4(1.0f),(float)(90*M_PI/180),vec3(0,1,0))* glm::scale(mat4(1.0f),vec3(2,2,2)));
        renderFence(glm::translate(mat4(1.0f),vec3(i,0,30))* glm::rotate(mat4(1.0f),(float)(90*M_PI/180),vec3(0,1,0))* glm::scale(mat4(1.0f),vec3(2,2,2)));
    }
    renderSwing(glm::translate(mat4(1.0f),vec3(-15,1.2,5))*glm::scale(mat4(1.0f),vec3(3,3,3)));


    renderParticleSystem(&ps);

    renderTruck(glm::translate(mat4(1.0f),vec3(-15,0,-8)));
}

void GLWidget::renderParticleSystem(ParticleSystem *ps) {
    glUseProgram(psProg);
    glBindVertexArray(psVao);

    glBindBuffer(GL_ARRAY_BUFFER, particleBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Particle)*ps->numParticles(), ps->data(), GL_DYNAMIC_DRAW);

    glDrawArrays(GL_POINTS, 0, ps->numParticles());
}


void GLWidget::renderSwing(glm::mat4 transform){
    renderCube(transform*glm::translate(mat4(1.0f),vec3(-.5,0,0))*glm::scale(mat4(1.0f),vec3(.1,1,.1)));
    renderCube(transform*glm::translate(mat4(1.0f),vec3(.5,0,0))*glm::scale(mat4(1.0f),vec3(.1,1,.1)));
    renderCube(transform*glm::translate(mat4(1.0f),vec3(0,0.5,0))*glm::scale(mat4(1.0f),vec3(1,.1,.1)));

    renderCube(transform*glm::translate(mat4(1.0f),vec3(.2,.25,swingTrans*.3)) * glm::rotate(mat4(1.0f),(float)(swingDegree*M_PI/180),vec3(1,0,0))*glm::scale(mat4(1.0f),vec3(.05,.6,.05)));
    renderCube(transform*glm::translate(mat4(1.0f),vec3(-.2,.25,swingTrans*.3))* glm::rotate(mat4(1.0f),(float)(swingDegree*M_PI/180),vec3(1,0,0))*glm::scale(mat4(1.0f),vec3(.05,.6,.05)));

    renderCube(transform*glm::translate(mat4(1.0f),vec3(0,0,swingTrans))* glm::rotate(mat4(1.0f),(float)(swingDegree*M_PI/180),vec3(1,0,0))*glm::scale(mat4(1.0f),vec3(.5,.1,.3)));

}

void GLWidget::renderDesk(mat4 transform){
    treeTex->bind();
    mat4 legscale =  glm::scale(mat4(1.0f),vec3(.2,2,.2));
    mat4 facescale =  glm::scale(mat4(1.0f),vec3(2.4,.2,1.4));
    mat4 trans = transform * glm::translate(mat4(1.0f),vec3(-1,0,-.5));
    mat4 trans1 = transform * glm::translate(mat4(1.0f),vec3(1,0,-.5));
    mat4 trans2 = transform * glm::translate(mat4(1.0f),vec3(-1,0,.5));
    mat4 trans3 = transform * glm::translate(mat4(1.0f),vec3(1,0,.5));
    renderCube(trans*legscale);
    renderCube(trans1*legscale);
    renderCube(trans2*legscale);
    renderCube(trans3*legscale);
    mat4 trans4 = transform* glm::translate(mat4(1.0f),vec3(0,1,0));
    renderCube(trans4 * facescale);
}

void GLWidget::renderChair(mat4 transform){
    treeTex->bind();
    mat4 legscale =  glm::scale(mat4(1.0f),vec3(.2,1,.2));
    mat4 facescale =  glm::scale(mat4(1.0f),vec3(1.3,.2,1.3));
    mat4 backScale = glm::scale(mat4(1.0f),vec3(1.3,1,.2));
    mat4 trans = transform * glm::translate(mat4(1.0f),vec3(-.5,0,-.5));
    mat4 trans1 = transform * glm::translate(mat4(1.0f),vec3(.5,0,-.5));
    mat4 trans2 = transform * glm::translate(mat4(1.0f),vec3(-.5,0,.5));
    mat4 trans3 = transform * glm::translate(mat4(1.0f),vec3(.5,0,.5));
    renderCube(trans*legscale);
    renderCube(trans1*legscale);
    renderCube(trans2*legscale);
    renderCube(trans3*legscale);
    mat4 trans4 = transform* glm::translate(mat4(1.0f),vec3(0,.6,0));
    renderCube(trans4 * facescale);
    mat4 trans5 = transform* glm::translate(mat4(1.0f),vec3(0,1,0.5));
    renderCube(trans5 * backScale);
}

void GLWidget::renderBookShelf(mat4 transform){
    glUseProgram(cubeProg);

    mat4 board = glm::scale(mat4(1.0f),vec3(.5,.1,2));
    mat4 backBoard = glm::scale(mat4(1.0f),vec3(.1,3,2));
    mat4 standBoard = glm::scale(mat4(1.0f),vec3(.5,3,.1));
    mat4 trans1 = transform * glm::translate(mat4(1.0f),vec3(.25,1.3,0));
    mat4 trans2 = transform * glm::translate(mat4(1.0f),vec3(0,1.3,-1));
    mat4 trans3 = transform * glm::translate(mat4(1.0f),vec3(0,1.3,1));
    for(int i=0;i<8;i++){
        renderCube(transform*glm::translate(mat4(1.0f),vec3(0,-.7+i*.5,0))*board);
    }

    renderCube(trans1*backBoard);
    renderCube(trans2*standBoard);
    renderCube(trans3*standBoard);

}

void GLWidget::renderSnowmen(){

    glUseProgram(cubeProg);
    GLuint diffuseColorLoc = glGetUniformLocation(cubeProg, "diffuseColor");
    GLuint ambientColorLoc = glGetUniformLocation(cubeProg, "ambientColor");
    GLuint lightColorLoc = glGetUniformLocation(cubeProg, "lightColor");



    glUniform3f(ambientColorLoc, 0.05, 0.05, 0.05);
    glUniform3f(diffuseColorLoc, 1, 1, 1);
    glUniform3f(lightColorLoc, 1, 1, 1);

    float talls[] = {2,4,6,8,10,2,4,6,8,10};
    float degrees[] = {70,85,100,115,130,145,160,175,190,205};

    for(int i=0;i<10;i++){
        tall=talls[i];
        degree=degrees[i];
        move=.04*(i+1);
        mat4 trans = glm::translate(mat4(1.0f),vec3(i*3,0,-i*3));
        renderBase(trans);
    }
}

void GLWidget::renderOlaf() {
    glUseProgram(olafProg);
    glBindVertexArray(olafSmoothVao);
    mat4 trans=glm::translate(mat4(1.0f),vec3(10,14,10));
    mat4 scale= glm::scale(mat4(1.0f),vec3(.1,.1,.1));
    mat4 transform= trans * scale;

    glUniformMatrix4fv(olafModelMatrixLoc,1,false,value_ptr(transform*eulerLerp));
    glDrawElements(GL_TRIANGLES, numOlafIndices, GL_UNSIGNED_INT, 0);
}

void GLWidget::renderBunny() {
    glUseProgram(modelProg);
    glBindVertexArray(modelSmoothVao);
    mat4 trans=glm::translate(mat4(1.0f),vec3(-10,1,10));
    mat4 scale= glm::scale(mat4(1.0f),vec3(.1,.1,.1));
    mat4 transform= trans * scale;

    glUniformMatrix4fv(modelModelMatrixLoc,1,false,value_ptr(transform));
    glDrawElements(GL_TRIANGLES, numBunnyIndices, GL_UNSIGNED_INT, 0);
}

void GLWidget::renderBox(){
    glUseProgram(cubeProg);
    glBindVertexArray(cubeVao);

    GLuint uvBuffer;
    glGenBuffers(1, &uvBuffer);

    vec2 uvs[] = {
        // top
        vec2(0.5,0),
        vec2(0.5,0.5),
        vec2(1,0.5),
        vec2(1,0),

        // bottom
        vec2(0,0),
        vec2(0,0.5),
        vec2(0.5,0.5),
        vec2(0.5,0),

        // front
        vec2(1,0.5),
        vec2(0.5,.5),
        vec2(.5,1),
        vec2(1,1),

        // back
        vec2(1,1),
        vec2(1,0.5),
        vec2(0.5,.5),
        vec2(.5,1),

        // right
        vec2(0,1),
        vec2(0.5,1),
        vec2(.5,.5),
        vec2(0,0.5),

        // left
        vec2(0.5,1),
        vec2(.5,.5),
        vec2(0,0.5),
        vec2(0,1)
    };


    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(uvs), uvs, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    GLint uvIndex = glGetAttribLocation(cubeProg, "uv");
    glEnableVertexAttribArray(uvIndex);
    glVertexAttribPointer(uvIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

    mat4 scale=glm::scale(mat4(1.0f),vec3(2,2,2));
    mat4 trans= glm::translate(mat4(1.0f),vec3(-5,.5,-5));

    boxTex->bind();
    renderCube(trans*scale);


    vec2 uvs1[] = {
        // top
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // bottom
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // front
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),
        vec2(0,0),

        // back
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0),

        // right
        vec2(1,1),
        vec2(1,0),
        vec2(0,0),
        vec2(0,1),

        // left
        vec2(0,0),
        vec2(0,1),
        vec2(1,1),
        vec2(1,0)

    };


    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(uvs1), uvs1, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    glEnableVertexAttribArray(uvIndex);
    glVertexAttribPointer(uvIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);
}

void GLWidget::renderGround(){

    glUseProgram(cubeProg);
    GLuint diffuseColorLoc = glGetUniformLocation(cubeProg, "diffuseColor");
    GLuint ambientColorLoc = glGetUniformLocation(cubeProg, "ambientColor");

    glUniform3f(ambientColorLoc, 0.05, 0.05, 0.05);
    glUniform3f(diffuseColorLoc, 0, 1, 1);
    mat4 scale=glm::scale(mat4(1.0f),vec3(100,.01,100));
    mat4 trans= glm::translate(mat4(1.0f),vec3(0,-.45,0));
    tex->bind();
    renderCube(trans*scale);
}

void GLWidget::renderHouse(){
    glUseProgram(cubeProg);
    GLuint diffuseColorLoc = glGetUniformLocation(cubeProg, "diffuseColor");
    GLuint ambientColorLoc = glGetUniformLocation(cubeProg, "ambientColor");

    GLuint lightColorLoc = glGetUniformLocation(cubeProg, "lightColor");

    glUniform3f(lightColorLoc, 1, 1, 1);

    glUniform3f(ambientColorLoc, 0.05, 0.05, 0.05);
    glUniform3f(diffuseColorLoc, 1, 0, 1);

    mat4 scale=glm::scale(mat4(1.0f),vec3(15,10,20));
    mat4 trans= glm::translate(mat4(1.0f),vec3(10,4.5,10));
    brickTex->bind();
    renderCube(trans*scale);


    glUniform3f(diffuseColorLoc, 0, 1, 0);

    doorTex->bind();
    scale = glm::scale(mat4(1.0f),vec3(0.1,6,3));
    trans = glm::translate(mat4(1.0f),vec3(2.5,2,10));

    mat4 rot = glm::rotate(mat4(1.0f),(float)(M_PI),vec3(1,0,0));
    renderCube(trans*rot*scale);
}

void GLWidget::renderTree(){
    mat4 trans = glm::translate(mat4(1.0f),vec3(-8,1.8,0));
    mat4 scale = glm::scale(mat4(1.0f),vec3(1,4,1));
    treeTex->bind();
    renderCube(trans*scale);
    scale = glm::scale(mat4(1.0f),vec3(5,3,5));
    trans = glm::translate(trans,vec3(0,3.5,0));
    leavesTex->bind();
    renderCube(trans*scale);
    scale = glm::scale(mat4(1.0f),vec3(4,3,4));
    trans = glm::translate(trans,vec3(0,2,0));
    renderCube(trans*scale);
    scale = glm::scale(mat4(1.0f),vec3(3,3,3));
    trans = glm::translate(trans,vec3(0,2,0));
    renderCube(trans*scale);
}

void GLWidget::renderTruck(mat4 transform){
    glUseProgram(cubeProg);
    GLuint diffuseColorLoc = glGetUniformLocation(cubeProg, "diffuseColor");
    GLuint ambientColorLoc = glGetUniformLocation(cubeProg, "ambientColor");

    GLuint lightColorLoc = glGetUniformLocation(cubeProg, "lightColor");

    glUniform3f(lightColorLoc, 1, 1, 1);

    glUniform3f(ambientColorLoc, 0.05, 0.05, 0.05);
    glUniform3f(diffuseColorLoc, .1, .1, .8);

    mat4 wheelScale = glm::scale(mat4(1.0f),vec3(0.5,2,2));
    mat4 headBot = glm::scale(mat4(1.0f),vec3(5,2,5));
    mat4 headTop = glm::scale(mat4(1.0f),vec3(5,2,4));
    mat4 backBot = glm::scale(mat4(1.0f),vec3(5,0.1,8));
    mat4 backBoard = glm::scale(mat4(1.0f),vec3(5,2,.1));
    mat4 backLeftBoard = glm::scale(mat4(1.0f),vec3(.1,2,8));

    renderCube(transform* glm::translate(mat4(1.0f),vec3(-2,0,-3))*wheelScale);
    renderCube(transform* glm::translate(mat4(1.0f),vec3(2,0,-3))*wheelScale);
    renderCube(transform* glm::translate(mat4(1.0f),vec3(-2,0,3))*wheelScale);
    renderCube(transform* glm::translate(mat4(1.0f),vec3(2,0,3))*wheelScale);

    renderCube(transform* glm::translate(mat4(1.0f),vec3(0,2,3))*headBot);
    renderCube(transform* glm::translate(mat4(1.0f),vec3(0,4,1))*headTop);
    renderCube(transform* glm::translate(mat4(1.0f),vec3(0,1,-3))*backBot);
    renderCube(transform* glm::translate(mat4(1.0f),vec3(-2.5,2,-3))*backLeftBoard);
    renderCube(transform* glm::translate(mat4(1.0f),vec3(2.5,2,-3))*backLeftBoard);
    renderCube(transform* glm::translate(mat4(1.0f),vec3(0,2,-7))*backBoard);
    renderCube(transform* glm::translate(mat4(1.0f),vec3(0,2,1))*backBoard);

}

void GLWidget::renderFence(mat4 transform){
    renderCube(transform* glm::translate(mat4(1.0f),vec3(0,0,0))*glm::scale(mat4(1.0f),vec3(.1,.1,.5)));
    renderCube(transform* glm::translate(mat4(1.0f),vec3(0,.4,0))*glm::scale(mat4(1.0f),vec3(.1,.1,.5)));
    renderCube(transform* glm::translate(mat4(1.0f),vec3(0,0,0))*glm::scale(mat4(1.0f),vec3(.1,1,.1)));
}

void GLWidget::renderCube(mat4 transform) {
    glUseProgram(cubeProg);
    glBindVertexArray(cubeVao);
    glUniformMatrix4fv(cubeModelMatrixLoc,1,false,value_ptr(transform));
    glDrawElements(GL_TRIANGLE_FAN, 29, GL_UNSIGNED_INT, 0);
}


void GLWidget::renderBase(glm::mat4 transform){
    mat4 scale=glm::scale(mat4(1.0f),vec3(1.5,1.0,1.5));
    snowTex->bind();
    renderCube(transform*scale);
    renderBody(transform);
}

void GLWidget::renderBody(mat4 transform){
    mat4 scale=glm::scale(mat4(1.0f),vec3(1,tall,1));
    mat4 trans = glm::translate(mat4(1.0f),vec3(0,(float)(0.5+tall/2),0));
    renderCube(transform*trans*scale);
    renderHead(transform);
    renderLeftArm(transform);
    renderRightArm(transform);
    renderButtons(transform);
}

void GLWidget::renderButtons(mat4 transform){
    mat4 scale = glm::scale(mat4(1.0),vec3(.2,.2,.2));
    for(int i=0;i<(tall-.5)/.5;i++){
        mat4 trans = glm::translate(mat4(1.0f),vec3(0,i*0.5+1.1,-0.5));
        renderCube(transform*trans*scale);
    }
}

void GLWidget::renderLeftArm(mat4 transform){
    mat4 scale=glm::scale(mat4(1.0f),vec3(.3,1.5,.3));
    mat4 trans = glm::translate(mat4(1.0f),vec3(1,tall+move,0));
    mat4 rot = glm::rotate(mat4(),(float)(degree*M_PI/360),vec3(0,0,1));
    renderCube(transform*trans*rot*scale);
}

void GLWidget::renderRightArm(mat4 transform){
    mat4 scale=glm::scale(mat4(1.0f),vec3(.3,1.5,.3));
    mat4 trans = glm::translate(mat4(1.0f),vec3(-1,tall+move,0));
    mat4 rot = glm::rotate(mat4(),(float)(-degree*M_PI/360),vec3(0,0,1));
    renderCube(transform*trans*rot*scale);
}

void GLWidget::renderHead(mat4 transform){
    mat4 scale = glm::scale(mat4(1.0f),vec3(.75,.75,.75));
    mat4 trans = glm::translate(mat4(1.0f),vec3(0,(.5+tall+.375),0));
    renderCube(transform*trans*scale);
    renderMouth(transform);
    renderLeftEye(transform);
    renderRightEye(transform);
}

void GLWidget::renderMouth(mat4 transform){
    mat4 scale = glm::scale(mat4(1.0f),vec3(.5,.2,.2));
    mat4 trans = glm::translate(mat4(1.0f),vec3(0,(.5+tall+.375-.175),-.5));
    renderCube(transform * trans * scale);
}

void GLWidget::renderLeftEye(mat4 transform){
    mat4 scale = glm::scale(mat4(1.0f),vec3(.2,.2,.2));
    mat4 trans = glm::translate(mat4(1.0f),vec3(0.2,.5+tall+.375+.125,-.5));
    renderCube(transform*trans*scale);
}

void GLWidget::renderRightEye(mat4 transform){
    mat4 scale = glm::scale(mat4(1.0f),vec3(.2,.2,.2));
    mat4 trans = glm::translate(mat4(1.0f),vec3(-0.2,.5+tall+.375+.125,-.5));
    renderCube(transform*trans*scale);
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(vertf);
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    std::string vertSTLString = vertString.toStdString();

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(fragf);
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    std::string fragSTLString = fragString.toStdString();

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);
    {
        GLint linked;
        glGetProgramiv( program, GL_LINK_STATUS, &linked );
        if ( !linked ) {
            GLsizei len;
            glGetProgramiv( program, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetProgramInfoLog( program, len, &len, log );
            std::cout << "Shader linker failed: " << log << std::endl;
            delete [] log;
        }
    }

    return program;
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_W:
            // forward
            forward=true;
            break;
        case Qt::Key_A:
            // left
            left=true;
            break;
        case Qt::Key_S:
            // back
            back=true;
            break;
        case Qt::Key_D:
            // right
            right=true;
            break;
        case Qt::Key_Tab:
            // toggle fly mode
            up=true;
            break;
        case Qt::Key_Shift:
            // down
            down=true;
            break;
        case Qt::Key_Space:
            // up or jump
            up=true;
            break;
        case Qt::Key_G:
            isWeatherOpen=!isWeatherOpen;
    }
}

void GLWidget::keyReleaseEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_W:
            // forward
            forward=false;
            break;
        case Qt::Key_A:
            // left
            left=false;
            break;
        case Qt::Key_S:
            // back
            back=false;
            break;
        case Qt::Key_D:
            // right
            right=false;
            break;
        case Qt::Key_Tab:
            // toggle fly mode
            up=false;
            break;
        case Qt::Key_Shift:
            // down
            down=false;
            break;
        case Qt::Key_Space:
            // up or jump
            up=false;
            break;
    }
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    lastPt = pt;
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    vec2 d = pt-lastPt;

    yaw-= d.x/100;
    pitch-=d.y/100;

    if(pitch>M_PI/2){
        pitch=M_PI/2;
    }else if(pitch<-M_PI/2){
        pitch=-M_PI/2;
    }

    yawMatrix = glm::rotate(mat4(1.0f),yaw,vec3(0,1,0));
    pitchMatrix=glm::rotate(mat4(1.0f),pitch,vec3(1,0,0));

    orientation = yawMatrix * pitchMatrix;
    updateView();

    // Part 1 - use d.x and d.y to modify your pitch and yaw angles
    // before constructing pitch and yaw rotation matrices with them

    lastPt = pt;
    update();
}

void GLWidget::updateView(){
    if(position.x>100){
        position.x=100;
    }else if(position.x<-100){
        position.x=-100;
    }

    if(position.y>100){
        position.y=100;
    }else if(position.y<-100){
        position.y=-100;
    }

    mat4 view = glm::lookAt(vec3(0,0,-5),vec3(0,0,0),vec3(0,1,0));
    mat4 trans = glm::translate(view,position);
    viewMatrix = inverse(trans*orientation);
    glUseProgram(cubeProg);
    glUniformMatrix4fv(cubeViewMatrixLoc,1,false,value_ptr(viewMatrix));

    glUseProgram(modelProg);
    glUniformMatrix4fv(modelViewMatrixLoc,1,false,value_ptr(viewMatrix));

    glUseProgram(olafProg);
    glUniformMatrix4fv(olafViewMatrixLoc,1,false,value_ptr(viewMatrix));

    glUseProgram(psProg);
    glUniformMatrix4fv(psViewMatrixLoc,1,false,value_ptr(viewMatrix));
}
