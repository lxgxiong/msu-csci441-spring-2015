#include "glwidget.h"
#include <iostream>

using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) {
}

GLWidget::~GLWidget() {
}

void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);


    cout << "Enter 3 points (enter a point as x y r g b, put a blank between each number):" << endl;

    Point points[3];
    Color colors[3];

    cin >> points[0].x >> points[0].y >> colors[0].r >> colors[0].g >> colors[0].b;
    cin >> points[1].x >> points[1].y >> colors[1].r >> colors[1].g >> colors[1].b;
    cin >> points[2].x >> points[2].y >> colors[2].r >> colors[2].g >> colors[2].b;

    cout << "You entered:" << endl;
    cout << points[0].x << ","<< points[0].y << ":" << colors[0].r << "," << colors[0].g << ","<< colors[0].b << endl;
    cout << points[1].x << ","<< points[1].y << ":" << colors[1].r << "," << colors[1].g << ","<< colors[1].b << endl;
    cout << points[2].x << ","<< points[2].y << ":" << colors[2].r << "," << colors[2].g << ","<< colors[2].b << endl;


    // position data for a single triangle
    Point pt1 = GLWidget::w2nd(points[0]);
    Point pt2 = GLWidget::w2nd(points[1]);
    Point pt3 = GLWidget::w2nd(points[2]);

    Point pts[3] = {
        pt1,pt2,pt3};

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    // Upload the position data to the GPU, storing
    // it in the buffer we just created.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders();

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information 
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

    GLuint colorBuffer;
    glGenBuffers(1,&colorBuffer);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    GLint colorIndex = glGetAttribLocation(program, "color");
    glEnableVertexAttribArray(colorIndex);
    glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);
    //GLWidget::resize(1024,768);
}

void GLWidget::resizeGL(int w, int h) {
    glViewport(0,0,w,h);
}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

GLuint GLWidget::loadShaders() {
    GLuint program = glCreateProgram();

    const GLchar* vertSource = 
        "#version 330\n"
        "in vec2 position;\n"
        "in vec4 color;\n"
        "out vec4 v_out_color;\n"
        "void main() {\n"
        "  gl_Position = vec4(position.x, position.y, 0, 1);\n"
        "  v_out_color = vec4(color.r, color.g, color.b,1);\n"
        "}\n";

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);
    
    const GLchar* fragSource = 
        "#version 330\n"
        "out vec4 color_out;\n"
        "in vec4 v_out_color;\n"
        "void main() {\n"
        "  color_out = vec4(v_out_color.r, v_out_color.g, v_out_color.b, 1.0);\n"
        "}\n";
    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);
    glUseProgram(program);

    return program;
}

Point GLWidget::w2nd(Point pt_w) {
    /* convert pt_w to normalized device coordinates */
    /* use this method to convert your input coordinates to
       normalized device coordinates */
    float xnd,ynd;
    qreal pixelRatio = this->devicePixelRatio();
    float width = 640/pixelRatio;
    float height = 480/pixelRatio;
    xnd = -1+pt_w.x*(2/width);
    ynd = 1-pt_w.y*(2/height);
    pt_w.x = xnd;
    pt_w.y = ynd;
    cout << "x:" << xnd << "y:" << ynd << endl;
    return pt_w;
}


